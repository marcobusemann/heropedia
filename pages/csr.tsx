import type { NextPage } from 'next'
import React, { useEffect, useState } from 'react'
import { FavoriteHeroesSlideover } from '../src/components/FavoriteHeroesSlideover';
import { HeroCard } from '../src/components/HeroCard';
import { HeroGrid } from '../src/components/HeroGrid';
import { Layout } from '../src/components/Layout'
import { Spinner } from '../src/components/Spinner';
import { SpinnerSvg } from '../src/components/SpinnerSvg';
import { Title } from '../src/components/Title';
import { TitleBar } from '../src/components/TitleBar';
import { Hero } from '../src/domain/Hero';
import { fetchHeroes } from '../src/fetchHeroes';
import { useFavoriteHeroesSlideover } from '../src/hooks/useFavoriteHeroesSlideover';

const Home: NextPage = () => {
  const { slideoverProps, buttonProps, isFavorite, toggleFavorite } = useFavoriteHeroesSlideover();

  const [heroes, setHeroes] = useState<Hero[] | null>(null);

  useEffect(() => {
    fetchHeroes()
      .then((r) => setHeroes(r))
      .catch(error => console.error(error))
  }, []);

  return (
    <Layout>
      <TitleBar favoritesButtonProps={buttonProps}>
        First 100 superheroes (Fetched on client)
      </TitleBar>

      <FavoriteHeroesSlideover {...slideoverProps} />

      {
        heroes && (
          <HeroGrid>
            {heroes.map((hero) => (
              <HeroCard key={hero.id} hero={hero} favorite={isFavorite(hero)} onToggleFavorite={toggleFavorite} />
            ))}
          </HeroGrid>
        )
      }

      {
        !heroes && (
          <Spinner />
        )
      }
    </Layout>
  )
}

export default Home
