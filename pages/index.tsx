import type { NextPage } from 'next'
import React from 'react'
import { Layout } from '../src/components/Layout'
import Link from 'next/link'

const Home: NextPage = () => {
  return (
    <Layout>

      <ul className="text-xl font-bold text-center">
        <li><Link href="/csr" prefetch={false}><a className="text-blue-500 hover:text-blue-300">CSR - Client side rendering</a></Link></li>
        <li><Link href="/ssr" prefetch={false}><a className="text-blue-500 hover:text-blue-300">SSR - Server side rendering</a></Link></li>
        <li><Link href="/sg" prefetch={false}><a className="text-blue-500 hover:text-blue-300">SG - Static generation</a></Link></li>
      </ul>
      
    </Layout>
  )
}

export default Home
