import type { GetStaticProps, NextPage } from 'next'
import React from 'react'
import { FavoriteHeroesSlideover } from '../src/components/FavoriteHeroesSlideover';
import { HeroCard } from '../src/components/HeroCard';
import { HeroGrid } from '../src/components/HeroGrid';
import { Layout } from '../src/components/Layout'
import { TitleBar } from '../src/components/TitleBar';
import { Hero } from '../src/domain/Hero';
import { fetchHeroes } from '../src/fetchHeroes';
import { useFavoriteHeroesSlideover } from '../src/hooks/useFavoriteHeroesSlideover';

type Props = {
    heroes: Hero[];
}

const Home: NextPage<Props> = ({ heroes }) => {
    const { slideoverProps, buttonProps, isFavorite, toggleFavorite } = useFavoriteHeroesSlideover();

    return (
        <Layout>
            <TitleBar favoritesButtonProps={buttonProps}>
                First 100 superheroes (SG)
            </TitleBar>

            <FavoriteHeroesSlideover {...slideoverProps} />

            <HeroGrid>
                {heroes.map((hero) => (
                    <HeroCard key={hero.id} hero={hero} favorite={isFavorite(hero)} onToggleFavorite={toggleFavorite} />
                ))}
            </HeroGrid>
        </Layout>
    )
}

export const getStaticProps: GetStaticProps = async () => {
    const heroes = await fetchHeroes()
        .catch(error => console.error(error))

    return {
        props: {
            heroes,
        }
    }
}

export default Home
