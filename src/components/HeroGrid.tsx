export const HeroGrid: React.FC = ({ children }) => (
    <ul role="list" className="grid grid-cols-1 gap-6 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4">
        {children}
    </ul>
)

export default HeroGrid;