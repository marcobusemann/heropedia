import React from "react"
import Link from 'next/link'

export const Layout: React.FC = ({ children }) => {
  return (
    <div>
        <nav className="flex items-center w-full shadow-lg min-h-[60px] p-6 bg-red-500 text-white">
            <Link href="/"><a><span className="font-bold text-2xl">HeroPedia</span></a></Link>
        </nav>


        <main className="container mx-auto my-10 px-10 text-gray-700">
            {children}
        </main>
    </div>
  )
}
