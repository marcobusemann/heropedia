import { Hero } from "../domain/Hero"
import { HeartIcon as HeartIconOutline } from '@heroicons/react/outline'
import { HeartIcon as HeartIconSolid } from '@heroicons/react/solid'

type Props = {
    hero: Hero;
    favorite: boolean;
    onToggleFavorite: (hero: Hero) => void;
}

export const HeroCard = ({ hero, favorite, onToggleFavorite }: Props) => (
    <li
        className={`flex flex-col text-center bg-white rounded-lg shadow`}
    >
        <div className="flex-1 flex flex-col p-8">
            <div className="relative w-32 h-32 flex-shrink-0 mx-auto">
                <img className="absolute w-32 h-32 rounded-full object-cover" src="/avatar.svg" alt={hero.name} />
                <img className="absolute w-32 h-32 rounded-full object-cover" placeholder="/avatar.svg" loading="lazy" src={hero.imageUrl} alt={hero.name} />
            </div>
            <h3 className="mt-6 text-gray-900 text-md font-medium">{hero.name}</h3>
            <div>
                <button type="button" className="w-11 h-11 p-1" onClick={() => onToggleFavorite(hero)}>
                    {
                        favorite ? <HeartIconSolid className="text-red-500" /> : <HeartIconOutline className="text-red-500" />
                    }
                </button>
            </div>
        </div>
    </li>
)

export default HeroCard;