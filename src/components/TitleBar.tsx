import React, { ButtonHTMLAttributes } from "react";
import { Title } from "./Title";

type Props = {
    favoritesButtonProps: ButtonHTMLAttributes<HTMLButtonElement>
}

export const TitleBar: React.FC<Props> = ({ children, favoritesButtonProps }) => (
    <div className="flex mb-10">
        <Title>{children}</Title>
        <div className="flex-grow" />
        <button type="button" className="block border rounded-lg border-gray-600 px-3 py-1 hover:bg-gray-300" {...favoritesButtonProps}>Favourites</button>
    </div>
)