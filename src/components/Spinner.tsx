import React from "react";
import { SpinnerSvg } from "./SpinnerSvg";

export const Spinner = () => <SpinnerSvg className="text-red-900 w-32 h-32 mx-auto" />