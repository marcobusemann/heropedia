import { Fragment, useState } from 'react'
import { Dialog, Transition } from '@headlessui/react'
import { XIcon } from '@heroicons/react/outline'
import { Hero } from '../domain/Hero'

type SlideoverProps = {
    title: string;
    open: boolean;
    onClose: () => void;
}

export const Slideover: React.FC<SlideoverProps> = ({ title, open, onClose, children }) => (
    <Transition.Root show={open} as={Fragment}>
        <Dialog as="div" className="fixed inset-0 overflow-hidden" onClose={onClose}>
            <div className="absolute inset-0 overflow-hidden">
                <Transition.Child
                    as={Fragment}
                    enter="ease-in-out duration-500"
                    enterFrom="opacity-0"
                    enterTo="opacity-100"
                    leave="ease-in-out duration-500"
                    leaveFrom="opacity-100"
                    leaveTo="opacity-0"
                >
                    <Dialog.Overlay className="absolute inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
                </Transition.Child>

                <div className="fixed inset-y-0 right-0 pl-10 max-w-full flex">
                    <Transition.Child
                        as={Fragment}
                        enter="transform transition ease-in-out duration-500 sm:duration-700"
                        enterFrom="translate-x-full"
                        enterTo="translate-x-0"
                        leave="transform transition ease-in-out duration-500 sm:duration-700"
                        leaveFrom="translate-x-0"
                        leaveTo="translate-x-full"
                    >
                        <div className="w-screen max-w-md">
                            <div className="h-full flex flex-col py-6 bg-white shadow-xl overflow-y-scroll">
                                <div className="px-4 sm:px-6">
                                    <div className="flex items-start justify-between">
                                        <Dialog.Title className="text-lg font-medium text-gray-900">{title}</Dialog.Title>
                                        <div className="ml-3 h-7 flex items-center">
                                            <button
                                                type="button"
                                                className="bg-white rounded-md text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                                onClick={() => onClose()}
                                            >
                                                <span className="sr-only">Close panel</span>
                                                <XIcon className="h-6 w-6" aria-hidden="true" />
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div className="mt-6 relative flex-1 px-4 sm:px-6">
                                    {children}
                                </div>
                            </div>
                        </div>
                    </Transition.Child>
                </div>
            </div>
        </Dialog>
    </Transition.Root>
)

type Props = {
    heroes: Hero[]
    open: boolean;
    onClose: () => void;
}

export const FavoriteHeroesSlideover = ({ open, onClose, heroes }: Props) => {
    return (
        <Slideover open={open} onClose={onClose} title="Favourites">
            <ul role="list" className="divide-y divide-gray-200">
                {heroes.map((hero) => (
                    <li key={hero.id} className="py-4 flex items-center">
                        <img className="h-16 w-16 rounded-full" src={hero.imageUrl} alt={hero.name} />
                        <div className="ml-3">
                            <p className="text-lg font-medium text-gray-900">{hero.name}</p>
                        </div>
                    </li>
                ))}
            </ul>
        </Slideover>
    )
}

export default FavoriteHeroesSlideover;