import { HTMLAttributes } from "react";

export const Title: React.FC<HTMLAttributes<HTMLElement>> = (props) => (
    <h1 className="text-2xl font-bold" {...props} />
)