export type Hero = {
    id: string;
    name: string;
    imageUrl: string;
}