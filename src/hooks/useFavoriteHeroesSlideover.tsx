import { useCallback, useState } from "react"
import { Hero } from "../domain/Hero";

export const useFavoriteHeroesSlideover = () => {
    const [open, setOpen] = useState(false);
    const [heroes, setHeroes] = useState<Hero[]>([]);

    const closeSlideover = useCallback(() => {
        setOpen(false);
    }, []);

    const openSlideover = useCallback(() => {
        setOpen(true);
    }, []);

    const addHero = useCallback((hero: Hero) => {
        setHeroes((oldHeroes) => [...oldHeroes, hero]);
    }, [])

    const isFavorite = (hero: Hero) => {
        return heroes.some(h => h.id === hero.id);
    }

    const toggleFavorite = useCallback((hero: Hero) => {
        setHeroes((oldHeroes) => {
            if (oldHeroes.some(h => h.id === hero.id))
                return [...oldHeroes.filter(h => h.id !== hero.id)];
            else 
                return [...oldHeroes, hero];
        });
    }, []);

    return {
        slideoverProps: {
            open,
            onClose: closeSlideover,
            heroes,
        },
        buttonProps: {
            onClick: openSlideover,
        },
        addHero,
        isFavorite,
        toggleFavorite,
    }
}