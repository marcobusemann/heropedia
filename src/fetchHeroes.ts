import { Hero } from "./domain/Hero"

export const fetchHeroes = async (amount: number = 100): Promise<Hero[]> => {
    return fetch(`https://akabab.github.io/superhero-api/api/all.json`)
        .then(r => r.json())
        .then<Hero[]>((r: any[]) => r.slice(0, amount).map((rh: any) => ({
            id: rh.slug,
            name: rh.name,
            imageUrl: rh.images.sm,
        })))
}